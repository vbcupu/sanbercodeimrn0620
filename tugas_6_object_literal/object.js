console.log('soal 1')
function arrayToObject(arr) {
    // Code di sini
   obj = {firstName:'', lastName:'', gender:'', age:''}
   tahunini = new Date()
   tahunini = tahunini.getFullYear()
   if (arr.length == 0){
       console.log(obj)
       return
   }
   for (let i=0; i<arr.length; i++){
       umur = arr[i][3]
       if (umur == undefined || umur > tahunini){
           umur= "Invalid Birth Year"
       }else{
           umur = tahunini - umur
       }
       obj.firstName = arr[i][0]
       obj.lastName = arr[i][1]
       obj.gender = arr[i][2]
       obj.age = umur
       console.log(obj)
   }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]

//console.log(people.length)
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


console.log('soal 2')
objbarang = [{
      namabarang : 'Sepatu brand Stacattu',
      harga : 1500000
  },{
      namabarang : 'Baju brand Zoro',
      harga : 500000
  }, 
  {
      namabarang : 'Sweater brand',
      harga : 175000
  },
  {
      namabarang : 'Casing Handphone',
      harga : 50000
  }] 


function shoppingTime(memberId='', money=0) {
  // you can only write your code here!
    var  listPurchased=[]
    let bayar = money
    var sisa = parseFloat(money)
    tampung = 0
    let i=0
    if (memberId ==''){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }else if(money < 50000){
        return 'Mohon maaf, uang tidak cukup'
    }else{
        while (i < objbarang.length){
            if (sisa > objbarang[i]['harga']) {
                sisa = sisa - objbarang[i]['harga']
                listPurchased.push(objbarang[i]['namabarang'])
            }
            i++
        }
    }
    let objshop = {}
    objshop = {
        memberId:memberId,
        money:money,
        listPurchased:listPurchased,
        changeMoney:sisa
    }
    return objshop
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('soal 3')
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  tarif=2000
  //your code here  
  objpen = {}
  arr = []  
  for (let i=0; i < arrPenumpang.length; i++) {         
      objpen = {
          penumpang:arrPenumpang[i][0],
          naikDari:arrPenumpang[i][1],
          tujuan:arrPenumpang[i][2],
          bayar: (rute.indexOf(arrPenumpang[i][2])-rute.indexOf(arrPenumpang[i][1])) * tarif
      }
      arr.push(objpen)
  }
  return(arr)
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]