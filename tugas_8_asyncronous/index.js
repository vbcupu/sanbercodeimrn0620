// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let times = 10000;
/*
readBooks(times, books[0], function (sisawaktu) {
  readBooks(sisawaktu, books[1], function (sisawaktu) {
    readBooks(sisawaktu, books[2], function () {});
  });
});
*/

let i = 0;

function baca(sisa){
    if (i<=books.length){
        if (sisa>=books[i].timeSpent){
            i++
            readBooks(sisa, books[i], baca)
        }
    }
}
readBooks(times, books[i], baca);
