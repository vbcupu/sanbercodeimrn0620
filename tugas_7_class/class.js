class Animal {
    // Code class di sini
    constructor( name, legs=4, cold_blooded='false' ) {
        this.legs = legs
        this.cold_blooded = cold_blooded        
        this.name = name
    }  
}
 
var sheep = new Animal("shaun");
 
console.log("1. Release 0")
console.log("---------------------------------")
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog di sini
class Ape extends Animal {
   yell(){
       console.log('Auooo')
   }
}
 
class Frog extends Animal {
    jump(){
        console.log('hop hop')
    }
 }

console.log("1. Release 1")
console.log("---------------------------------")
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")


kodok.jump() // "hop hop" 

class Clock {
    constructor( temp ) {
      this.template = temp.template
      this.timer = undefined
    }
  
    render() {
      var date 	  = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render()
      this.timer = setInterval( this.render.bind(this) , 1000);
  }
  }
  
  console.log("2. Function to Class")
  console.log("---------------------------------")
  var clock = new Clock({template: 'h:m:s'});
  clock.start();
    