//SOAL 1
function teriak(){
    return 'Halo Sanbers!';
}
console.log('SOAL 1');
console.log(teriak());



//SOAL 2
var num1 = 12
var num2 = 4 
function kalikan(angka1, angka2){
    return angka1*angka2;
}
var hasilKali = kalikan(num1, num2)
console.log('SOAL 2');
console.log(hasilKali) // 48



//SOAL 3
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
function introduce (nama, age, address, hobby){
    return ("Nama saya ".concat(nama, ", umur saya ", age, ", alamat saya ", address, ", dan saya punya hobby yaitu ", hobby));
}
var perkenalan = introduce(name, age, address, hobby)
console.log('SOAL 3');
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
