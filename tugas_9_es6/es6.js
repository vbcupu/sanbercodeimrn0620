console.log('1. Arrow di ES6')
console.log('=============')
golden = () => {
    console.log ("this is golden!!")
}
golden()


console.log('2. Arrow di ES6')
console.log('=============')
newFunction = (firstName, lastName) => {
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: () => {
        console.log(firstName + " " + lastName);
      }          
  }
}      
//Driver Code 
newFunction("William", "Imoh").fullName()


console.log('3. Destructuring ES6')
console.log('=============')

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject
// Driver code
console.log(firstName, lastName, destination, occupation)


console.log('4. Array Spreading ES6 ') 
console.log('=============')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [ ...west, ...east ]
//Driver Code
console.log(combined)


console.log('5. Template Literals ES6')
console.log('=============')
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, '  
    'consectetur adipiscing elit, ${planet}  do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
     ad minim veniam`
 
// Driver Code
console.log(before) 
